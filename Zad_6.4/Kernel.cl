__kernel void Kernel(__global int* A, __global int* B, __global int* C,int size)
{
    int n = get_global_id(0);
	if (n < (size*size))
	{	 
		//WZOR: C[i][j]+=A[i][(i+k)%n]+B[(i+k)%n][j]
		int elem_A=0;
		int elem_B=0;
		int value=0;
		int wsp_x=n/size;
		int wsp_y=n%size;
		for (int i=0;i<size;i++) //size np.4
		{
			elem_A=A[(wsp_x*size)+(((wsp_x*size)+wsp_x+i)%size)];
			elem_B=B[((i+wsp_x)%size)*size+wsp_y];
			value+=elem_A*elem_B;
		}
		C[n]=value;
    }
}