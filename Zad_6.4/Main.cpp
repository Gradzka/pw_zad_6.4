#include <iostream>
#include <fstream>
#include <string>
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#include <CL/cl.h>

void randomizeArray(cl_int* matrix_vector, size_t p_size_matrix)
{
	for (size_t i = 0; i < p_size_matrix; ++i)
	{
		matrix_vector[i] = rand() % 10;
	}
}
void drukuj(cl_int* matrix_vector, size_t p_size_matrix, size_t one_side_of_matrix)
{
	for (size_t i = 0; i < p_size_matrix; i++)
	{
		printf("%d\t", matrix_vector[i]);
		if (((i + 1) % one_side_of_matrix) == 0)
			printf("\n");
	}
}
void zeruj(cl_int* matrix_vector, size_t p_size_matrix)
{
	for (size_t i = 0; i < p_size_matrix; ++i)
	{
		matrix_vector[i] = 0;
	}
}
void vector_copy_vector(cl_int* matrix_vector1,cl_int* matrix_vector2, size_t p_size_matrix)
{
	for (size_t i = 0; i < p_size_matrix; ++i)
	{
		matrix_vector2[i] = matrix_vector1[i];
	}
}
int main()
{
	cl_int error = CL_SUCCESS;

	// Get platform number.
	cl_uint platformNumber = 0; //zmienna zawiera liczbe dostepnych platform obslugujacych OpenCL,

	error = clGetPlatformIDs(0, NULL, &platformNumber); //0 - wykona sie poprawnie, rozne od 0 - blad

	if (0 == platformNumber)
	{
		std::cout << "No OpenCL platforms found." << std::endl;
		return 0;
	}

	// Get platform identifiers.
	cl_platform_id* platformIds = new cl_platform_id[platformNumber]; //lista identyfikatorow dostepnych platform

	error = clGetPlatformIDs(platformNumber, platformIds, NULL);//pobieranie listy identyfika...

	// Get device count.
	cl_uint deviceNumber;

	error = clGetDeviceIDs(platformIds[1], CL_DEVICE_TYPE_GPU, 0, NULL, &deviceNumber);//pobranie liczby urzadzen wspierajacych OpenCL

	if (0 == deviceNumber)
	{
		std::cout << "No OpenCL devices found on platform " << 1 << "." << std::endl;
	}

	// Get device identifiers.
	cl_device_id* deviceIds = new cl_device_id[deviceNumber];//lista identyfikatorow dostepnych urzadzen

	error = clGetDeviceIDs(platformIds[1], CL_DEVICE_TYPE_GPU, deviceNumber, deviceIds, &deviceNumber);//pobieranie listy identyfika...

	size_t one_side_of_matrix = 4;
	size_t p_size_matrix = one_side_of_matrix*one_side_of_matrix;

	// 1. allocate host memory for matrix A and power variable
	cl_int *A = new cl_int[p_size_matrix];
	cl_int *B = new cl_int[p_size_matrix];
	cl_int power = 0;

	// 2. initialize host memory
	randomizeArray(A, p_size_matrix);
	for (size_t i = 0; i < p_size_matrix; ++i)
	{
		B[i] = A[i];
	}
	// 3. print out A
	printf("Matrix A\n");
	drukuj(A, p_size_matrix, one_side_of_matrix);

	printf("\nPodaj potege, do ktorej chcesz podniesc macierz\n");
	while (power <= 0 || power>7)
	{
		scanf_s("%d", &power);
		if (power <= 0) printf("Podano niewlasciwa potege!\n");
		else if (power > 7)	printf("Dla podanej potegi zostanie przekroczony zakres cl_int podczas wykonywania obliczen!\n");
	}
	//printf("%d\n", power);
	// 1. allocate host memory for matrix C, fill C with 0, print out C
	cl_int *C = new cl_int[p_size_matrix];
	zeruj(C, p_size_matrix);
	//printf("\nMatrix C\n");
	//drukuj(C, p_size_matrix, one_side_of_matrix);

	// Create the OpenCL context.
	cl_context context = clCreateContext(0, deviceNumber, deviceIds, NULL, NULL, NULL);//utworzenie kontekstu, ktory umozliwi wykonywanie polecen na danym urzadzeniu

	if (NULL == context)
	{
		std::cout << "Failed to create OpenCL context." << std::endl;
	}

	// Create a command-queue
	cl_command_queue commandQueue = clCreateCommandQueue(context, deviceIds[0], 0, &error);//kolejka polecen na wybranym przez nas urzadzeniu
	// Allocate the OpenCL buffer memory objects for source and result on the device.
	//Alokacja pamieci na karcie graficznej dla wektorow
	cl_mem bufferA = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(cl_int) * p_size_matrix, NULL, &error);//tylko do odczytu
	cl_mem bufferB = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(cl_int) * p_size_matrix, NULL, &error);//tylko do odczytu
	cl_mem bufferC = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(cl_int) * p_size_matrix, NULL, &error);//tylko do zapisu

	// Read the OpenCL kernel in from source file.
	std::ifstream file(".\\Kernel.cl", std::ifstream::in);
	std::string str;

	str.assign(std::istreambuf_iterator<char>(file), std::istreambuf_iterator<char>());
	const char* source = str.c_str();

	// Create the program.
	cl_program program = clCreateProgramWithSource(context, 1, &source, NULL, &error);

	error = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);

	// Create the kernel.
	cl_kernel kernel = clCreateKernel(program, "Kernel", &error);//kernel to funkcja zdefiniowana w kodzie programu, ktory zaladowalismy na GPU

	// 7. Launch OpenCL kernel
	size_t localWorkSize = one_side_of_matrix;

	// Set the Argument values.
	error = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void*)&bufferA);
	error = clSetKernelArg(kernel, 1, sizeof(cl_mem), (void*)&bufferB);
	error = clSetKernelArg(kernel, 2, sizeof(cl_mem), (void*)&bufferC);
	error = clSetKernelArg(kernel, 3, sizeof(cl_int), (void*)&localWorkSize);

	for (size_t pow = 1; pow < power; pow++)
	{	
		// Asynchronous write of data to GPU device.
		error = clEnqueueWriteBuffer(commandQueue, bufferA, CL_FALSE, 0, sizeof(cl_int) * p_size_matrix, A, 0, NULL, NULL);
		error = clEnqueueWriteBuffer(commandQueue, bufferB, CL_FALSE, 0, sizeof(cl_int) * p_size_matrix, B, 0, NULL, NULL);

		// Launch kernel.
		error = clEnqueueNDRangeKernel(commandQueue, kernel, 1, NULL, &p_size_matrix, &localWorkSize, 0, NULL, NULL);
		//kernel zostanie uruchomiony tyle razy ile wynosi wartosc zmiennej p_size_matrix

		// Read back results and check accumulated errors.
		error = clEnqueueReadBuffer(commandQueue, bufferC, CL_TRUE, 0, sizeof(cl_int) * p_size_matrix, C, 0, NULL, NULL);
		if (pow != (power - 1))
		{
			vector_copy_vector(C, A, p_size_matrix);
			//zeruj(C, p_size_matrix);
		}
	}
	// Print results.
	std::cout << std::endl << "Wynik potegowania" << std::endl << "Matrix C" << std::endl;
	if (power == 1)
	{
		drukuj(A, p_size_matrix, one_side_of_matrix);
	}
	else
	{
		drukuj(C, p_size_matrix, one_side_of_matrix);
	}

	// Cleanup and free memory.
	clFlush(commandQueue);
	clFinish(commandQueue);
	clReleaseKernel(kernel);
	clReleaseProgram(program);
	clReleaseMemObject(bufferA);
	clReleaseMemObject(bufferB);
	clReleaseMemObject(bufferC);
	clReleaseCommandQueue(commandQueue);
	clReleaseContext(context);

	delete[] A;
	delete[] B;
	delete[] C;

	delete[] platformIds;
	delete[] deviceIds;

	// Press Enter, to quit application.
	std::cin.get();

	return 0;
}